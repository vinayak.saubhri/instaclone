import { View, Text, TextInput, Button, StyleSheet, Pressable, TouchableOpacity, Alert } from 'react-native'
import React from 'react'
import * as yup from 'yup';
import { Formik } from "formik";
import { validate } from 'email-validator';
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth, db } from '../../Firebase';
import { doc, setDoc } from "firebase/firestore";
const signUpFormSchema = yup.object().shape({
    email: yup.string().email().required('An email is Required'),
    userName: yup.string().required().min(2, 'Username is required'),
    password: yup.string().required().min(8, 'Passowrd should be of minimum 8 characters')
});

const SignupForm = ({ navigation }) => {


    const getRandomProfilePicture = async () => {
        const response = await fetch('https://randomuser.me/api');
        const data = await response.json();
        return data.results[0].picture.large
    }


    const onSignUp = async (email, password, userName) => {

        try {

            const userCredential = await createUserWithEmailAndPassword(auth, email, password)
            // Signed in 
            const user = userCredential.user;
            await setDoc(doc(db, "users", email), {
                owner_uid: user.uid,
                username: userName,
                email: email,
                profile_picture: await getRandomProfilePicture()
            });
            console.log(user);
            console.log("User registered succesfully");

        } catch (error) {
            console.log(error.message)
            Alert.alert('My Lord',
                error.message,)
        }
    }
    return (
        <View style={{ marginTop: 80 }}>
            <Formik

                initialValues={{ email: "", password: "", userName: "" }}
                onSubmit={async (values) => {
                    await onSignUp(values.email, values.password, values.userName)
                    console.log(values);
                }
                }
                validationSchema={signUpFormSchema}
                validateOnMount={true}
            >
                {({ handleBlur, handleChange, handleSubmit, values, errors, isValid }) =>

                    <>

                        <View style={[styles.inputField, { borderColor: 1 > values.userName.length || values.userName.length >= 2 ? "#ccc" : "red" }]}>
                            <TextInput
                                placeholderTextColor="#444"
                                placeholder='Username'
                                autoCapitalize='none'
                                autoFocus={true}
                                onChangeText={handleChange("userName")}
                                onBlur={handleBlur("userName")}
                                value={values.userName}
                            />
                        </View>
                        <View style={[styles.inputField, { borderColor: values.email.length < 1 || validate(values.email) ? "#ccc" : "red" }]}>
                            <TextInput
                                placeholderTextColor="#444"
                                placeholder='Email'
                                autoCapitalize='none'
                                keyboardType='email-address'
                                textContentType='emailAddress'
                                onChangeText={handleChange("email")}
                                onBlur={handleBlur("email")}
                                value={values.email}
                            />
                        </View>
                        <View style={[styles.inputField, {
                            borderColor: 1 > values.password.length || values.password.length >= 8 ? "#ccc" : "red"
                        }]}>
                            <TextInput

                                placeholderTextColor="#444"
                                placeholder='Password'
                                autoCapitalize='none'
                                textContentType='password'
                                autoCorrect={false}
                                secureTextEntry={true}
                                onChangeText={handleChange("password")}
                                onBlur={handleBlur("password")}
                                value={values.password}
                            />
                        </View>
                        <Pressable titleSize={20} style={styles.button(isValid)} onPress={handleSubmit} disabled={!isValid}>
                            <Text style={styles.buttonText}>
                                Sign Up
                            </Text>
                        </Pressable>

                        <View style={styles.signupContainer}>
                            <Text>Already have a account ? </Text>
                            <TouchableOpacity onPress={() => navigation.goBack()}>
                                <Text style={{ color: '#6bb0f5' }}>Log In</Text>
                            </TouchableOpacity>
                        </View>
                    </>
                }
            </Formik>
        </View>
    )
}

const styles = StyleSheet.create({
    inputField: {
        borderRadius: 4,
        padding: 12,
        backgroundColor: "#FAFAFA",
        marginBottom: 10,
        borderWidth: 1
    },
    button: isValid => ({
        backgroundColor: isValid ? '#0096f6' : "#9acaf7",
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: 42,
        borderRadius: 4
    })
    ,
    buttonText: {
        color: '#fff',
        fontWeight: '600',
        fontSize: 20
    },
    signupContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 50
    }
})
export default SignupForm