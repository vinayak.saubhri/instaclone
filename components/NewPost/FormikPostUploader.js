import { View, Text, Image, TextInput, Button } from 'react-native'
import React, { useState, useEffect } from 'react'
import * as yup from 'yup';
import { Formik, Field, Form } from "formik";
import { Divider } from 'react-native-elements';
import validUrl from 'valid-url';
import { doc, query, collection, where, getDocs, limit, setDoc, serverTimestamp, addDoc } from 'firebase/firestore';
import { auth, db } from '../../Firebase';


const PLACEHOLDER_IMAGE_URL = "https://shorturl.at/koEFQ"
const uploadPostSchema = yup.object().shape({
    imageUrl: yup.string().url().required('A URL is Required'),
    caption: yup.string().max(2200, 'Max limit 2200 characters')
});

const FormikPostUploader = ({ navigation }) => {
    const [currentUser, setCurrentUser] = useState(null);
    const [thumbnail, setThumbnail] = useState(PLACEHOLDER_IMAGE_URL);


    const getUserName = async () => {
        const user = auth.currentUser;
        const q = query(collection(db, "users"), where("owner_uid", "==", user.uid), limit(1));

        const querySnapshot = await getDocs(q);
        querySnapshot.forEach((doc) => {
            setCurrentUser({
                userName: doc.data().username,
                profile_picture: doc.data().profile_picture,
            })
        });
        // console.log(user);
    }


    const uploadPostToFirebase = async (imageUrl, caption) => {
        try {
            console.log(currentUser)
            const docRef = await addDoc(collection(db, "users", auth.currentUser.email, "posts"), {
                imageUrl: imageUrl,
                user: currentUser.userName,
                profile_picture: currentUser.profile_picture,
                owner_uid: auth.currentUser.uid,
                owner_email: auth.currentUser.email,
                caption: caption,
                likes_by_user: [],
                comments: [],
                createdAt: serverTimestamp()
            });
            console.log("Post created succesfully", docRef);
            navigation.goBack();
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getUserName();
        // uploadPostToFirebase();
    }, [])

    return (
        <Formik

            initialValues={{ caption: "", imageUrl: "" }}
            onSubmit={values => {

                uploadPostToFirebase(values.imageUrl, values.caption)
            }
            }
            validationSchema={uploadPostSchema}
            validateOnMount={true}
        >
            {({ handleBlur, handleChange, handleSubmit, values, errors, isValid }) =>

                <>
                    <View

                        style={{
                            margin: 20,
                            justifyContent: 'space-between',
                            flexDirection: 'row'
                        }}
                    >
                        <Image source={{
                            uri: validUrl.isUri(thumbnail) ? thumbnail : PLACEHOLDER_IMAGE_URL
                        }}
                            style={{ width: 100, height: 100 }}
                        />
                        <View

                            style={{ flex: 1, marginLeft: 12 }}
                        >

                            <TextInput
                                placeholder='Write a Caption...'
                                placeholderTextColor="gray"
                                multiline={true}
                                style={{ color: "white", fontSize: 18 }}
                                onChangeText={handleChange("caption")}
                                onBlur={handleBlur("caption")}
                                value={values.caption}
                            />
                            {errors.caption && (
                                <Text
                                    style={{ fontSize: 10, color: 'red' }}

                                >{errors.caption}</Text>
                            )}
                        </View>
                    </View>
                    <Divider width={1} orientation='vertical' />

                    <TextInput
                        onChange={e => setThumbnail(e.nativeEvent.text)}
                        style={{ color: "white" }}
                        placeholder='Enter Image Url'
                        placeholderTextColor="gray"
                        onChangeText={handleChange("imageUrl")}
                        onBlur={handleBlur("imageUrl")}
                        value={values.imageUrl}
                    />

                    {errors.imageUrl && (
                        <Text
                            style={{ fontSize: 10, color: 'red' }}

                        >{errors.imageUrl}</Text>
                    )}

                    <Button title='Share' onPress={handleSubmit} disabled={!isValid} />
                </>
            }
        </Formik >
    )
}

export default FormikPostUploader