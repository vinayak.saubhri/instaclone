// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBJYoZLA_J6cgGkwVUdB9uhYTugIz8mi_E",
  authDomain: "insta-1eadf.firebaseapp.com",
  projectId: "insta-1eadf",
  storageBucket: "insta-1eadf.appspot.com",
  messagingSenderId: "687856234585",
  appId: "1:687856234585:web:e2ee98f820530b27ad8148",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getFirestore(app);

export { db, auth };
