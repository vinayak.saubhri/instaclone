import { Text, View } from "react-native";
import AuthNavigation from "./AuthNavigation";

export default function App() {
  return <AuthNavigation />;
}
